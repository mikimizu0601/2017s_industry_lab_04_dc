package ictgradschool.industry.lab04.ex03;

import com.sun.org.apache.xpath.internal.SourceTree;
import ictgradschool.Keyboard;
/**
 * A guessing game!
 */
public class GuessingGame {

    public void start() {

        // TODO Write your code here.

    int goal = (int) (Math.random() * 100 + 1);
    int guess = 0;

    while (guess != goal) {
        System.out.println("Enter your guess (1 - 100): ");
        guess = Integer.parseInt(Keyboard.readInput());
        if (guess < goal) {
            System.out.println(guess + "Too low, try again");
        }
        else if (guess > goal) {
                System.out.println(guess + "Too high, try again");
            }
        else {
            System.out.println(guess + "Perfect!");
            System.out.println("Goodbye");
        }

        }

    }

    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {

        GuessingGame ex = new GuessingGame();
        ex.start();

    }
}
